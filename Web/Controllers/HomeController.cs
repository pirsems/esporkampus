﻿using DAL;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace Web.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Title = "Home Page";

            return View();
        }
        public ActionResult Single()
        {
            MyModel db = new MyModel();
            ViewBag.PageList = db.Universities.ToList();
            ViewBag.PageList = db.Games.ToList();
            return View();
        }
        public async Task<ActionResult> Users()
        {
            using (HttpClient client = new HttpClient())
            {
                string returnResult = await client.GetStringAsync("http://espor.pollyfy.com/api/ApiUsers");
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                List<Users> list = serializer.Deserialize<List<Users>>(returnResult);
                return View(list);
            }
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> UserAdd([Bind(Include = "UserId,TeamId,GameId,UniversityId,UserName,NickName,Email")] Users user, string Takim)
        {
            switch(Takim)
            {
                case "Evet":
                    //takimidye erişilecek
                case "Hayır":
                    user.GameId = 1;
                    break;

            }
            if (ModelState.IsValid)
            {
                MyModel db = new MyModel();
                ViewBag.PageList = db.Universities.ToList();

                if (user.UserName == null || user.Email == null || user.NickName == null)
                {
                    ViewBag.BosVeri = "Lütfen Boşlukları Doldunuz!";
                    return View();
                } 
                user.IsActive = true;
                user.CreatedDate = DateTime.Now;
                
                using (HttpClient client = new HttpClient())
                {
                    var data = JsonConvert.SerializeObject(user);
                    HttpContent content = new StringContent(data, System.Text.Encoding.UTF8, "application/json");
                    var returnResult = await client.PostAsync("http://espor.pollyfy.com/api/ApiUsers", content);
                }
                return RedirectToAction("Admins");
            }
            return RedirectToAction("Admins");
        }
    }
}
