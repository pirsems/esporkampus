﻿using DAL;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace Web.Controllers
{
    public class AdminController : Controller
    {
        // GET: Index ddd
        public ActionResult Index()
        {
            return View();
        }
        // GET: Blog
        public ActionResult Blog()
        {
            return View();
        }
        // GET: BlogAdd
        public ActionResult BlogAdd()
        {
            return View();
        }
        // GET: Users
        #region
        public async Task<ActionResult> Users()
        {
            using (HttpClient client = new HttpClient())
            {
                string returnResult = await client.GetStringAsync("http://espor.pollyfy.com/api/ApiUsers");
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                List<Users> list = serializer.Deserialize<List<Users>>(returnResult);
                return View(list);
            }
        }
        #endregion
        // GET: UserAdd
        #region
        public ActionResult UserAdd()
        {
            MyModel db = new MyModel();
            ViewBag.PageList = db.Universities.ToList();
            return View();
        }
        #endregion
        //POST: UserAdd
        #region
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> UserAdd([Bind(Include = "UserId,UserName,NickName,Email,Password,Role")] Users user, string Rol)
        {
            if (ModelState.IsValid)
            {
                MyModel db = new MyModel();
                ViewBag.PageList = db.Universities.ToList();

                if (user.UserName == null || user.Email == null || Rol == null)
                {
                    ViewBag.BosVeri = "Lütfen Boşlukları Doldunuz!";
                    return View();
                }
                switch (Rol)
                {
                    case "Sistem":
                        user.Role = 0;
                        break;
                    case "Editör":
                        user.Role = 1;
                        break;
                    case "İstatistik":
                        user.Role = 2;
                        break;
                }
                user.IsActive = true;
                user.CreatedDate = DateTime.Now;
                user.Wins = 0;
                user.Ties = 0;
                user.Loses = 0;
                using (HttpClient client = new HttpClient())
                {
                    var data = JsonConvert.SerializeObject(user);
                    HttpContent content = new StringContent(data, System.Text.Encoding.UTF8, "application/json");
                    var returnResult = await client.PostAsync("http://espor.pollyfy.com/api/ApiUsers", content);
                }
                return Redirect("/Admin/Users/");
            }
            return Redirect("/Admin/Users/");
        }
        #endregion
        // GET: UserEdit 
        #region 
        public ActionResult UserEdit(int id)
        {
            MyModel db = new MyModel();
            ViewBag.PageList = db.Universities.ToList();
            var br = db.Users.FirstOrDefault(a => a.UserId == id);
            return View(br);
        }

        #endregion
        // POST :UserEdit
        #region
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> UserEdit([Bind(Include = "UserId,UserName,NickName,Email,Password,Role")] int id, string Rol, Users user)
        {
            if (ModelState.IsValid)
            {
                MyModel db = new MyModel();
                ViewBag.PageList = db.Universities.ToList();
                if (user.UserName == null || user.Email == null || Rol == null)
                {
                    ViewBag.BosVeri = "Lütfen Boşlukları Doldunuz!";
                    return View();
                }
                switch (Rol)
                {
                    case "Sistem":
                        user.Role = 0;
                        break;
                    case "Editör":
                        user.Role = 1;
                        break;
                    case "İstatistik":
                        user.Role = 2;
                        break;
                }

                var br = db.Users.FirstOrDefault(a => a.UserId == id);

                br.Role = user.Role;
                user.IsActive = br.IsActive;
                br.UserName = user.UserName;
                br.Password = user.Password;
                br.NickName = user.NickName;
                br.Email = user.Email;
                br.UniversityId = user.Universities.UniversityId;


                using (HttpClient client = new HttpClient())
                {
                    var data = JsonConvert.SerializeObject(user);
                    HttpContent content = new StringContent(data, System.Text.Encoding.UTF8, "application/json");
                    string Url = "http://espor.pollyfy.com/api/ApiUsers";
                    var uri = new Uri(string.Format(Url, id));
                    var returnResult = await client.PutAsync(uri, content);
                }
                return Redirect("/Admin/Users/");
            }
            return Redirect("/Admin/Users/");
        }
        #endregion


        //DELETE: Users
        #region
        public async Task<ActionResult> UserDelete(int id)
        {
            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://espor.pollyfy.com/");
                await client.DeleteAsync("api/ApiUsers/"+id.ToString());
            }
            return Redirect("/Admin/Users/");
        }
        #endregion

        // GET: Messages
        public ActionResult Messages()
        {
            return View();
        }
        // GET: Settings
        public ActionResult Settings()
        {
            return View();
        }
    }
}