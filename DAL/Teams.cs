namespace DAL
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Teams
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Teams()
        {
            Blogs = new HashSet<Blogs>();
            Games = new HashSet<Games>();
        }

        [Key]
        public int TeamId { get; set; }

        public int? UserId { get; set; }

        public int? MatchId { get; set; }

        public int? GamesId { get; set; }

        public int? TeamDetailId { get; set; }

        [StringLength(100)]
        public string TeamName { get; set; }

        [StringLength(200)]
        public string LogoImageURL { get; set; }

        public bool? IsActive { get; set; }

        [Column(TypeName = "smalldatetime")]
        public DateTime? CreatedDate { get; set; }

        public int? Wins { get; set; }

        public int? Loses { get; set; }

        public int? Ties { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Blogs> Blogs { get; set; }

        public virtual Users Users { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Games> Games { get; set; }
    }
}
