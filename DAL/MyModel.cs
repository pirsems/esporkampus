namespace DAL
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class MyModel : DbContext
    {
        public MyModel()
            : base("name=MyModel")
        {
        }

        public virtual DbSet<Blogs> Blogs { get; set; }
        public virtual DbSet<Characters> Characters { get; set; }
        public virtual DbSet<Games> Games { get; set; }
        public virtual DbSet<Matches> Matches { get; set; }
        public virtual DbSet<Messages> Messages { get; set; }
        public virtual DbSet<Referees> Referees { get; set; }
        public virtual DbSet<Settings> Settings { get; set; }
        public virtual DbSet<sysdiagrams> sysdiagrams { get; set; }
        public virtual DbSet<Teams> Teams { get; set; }
        public virtual DbSet<Universities> Universities { get; set; }
        public virtual DbSet<Users> Users { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Games>()
                .HasMany(e => e.Teams)
                .WithMany(e => e.Games)
                .Map(m => m.ToTable("TeamsToGames").MapLeftKey("GameId").MapRightKey("TeamId"));

            modelBuilder.Entity<Games>()
                .HasMany(e => e.Users)
                .WithMany(e => e.Games)
                .Map(m => m.ToTable("UsersToGames").MapLeftKey("GameId").MapRightKey("UserId"));

            modelBuilder.Entity<Users>()
                .Property(e => e.UserName)
                .IsUnicode(false);
        }
    }
}
